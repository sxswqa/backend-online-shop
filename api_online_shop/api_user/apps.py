from django.apps import AppConfig
from api_online_shop.api_user import signals


class ApiUserConfig(AppConfig):
    name = 'api_user'

    def ready(self):
        import api_online_shop.api_user.signals
