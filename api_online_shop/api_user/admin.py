from django.contrib import admin

# Register your models here.
from api_online_shop.api_user.models import Profile

admin.site.register(Profile)