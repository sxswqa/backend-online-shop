# Generated by Django 3.0.5 on 2020-04-16 16:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name_plural': 'категории',
            },
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api_shop.Category')),
            ],
            options={
                'verbose_name_plural': 'подкатегории',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('price', models.IntegerField()),
                ('description', models.TextField()),
                ('image', models.ImageField(default='default.jpeg', upload_to='images/')),
                ('subcategory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api_shop.SubCategory')),
            ],
            options={
                'verbose_name_plural': 'товары',
            },
        ),
    ]
