from django.db import models


class Category(models.Model):
    """categories"""
    title = models.CharField(max_length=200)
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'категории'

    def __str__(self):
        return self.title


class SubCategory(models.Model):
    """subcategories menu"""
    category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'подкатегории'

    def __str__(self):
        return self.title


class Item(models.Model):
    """items"""
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    price = models.IntegerField()
    description = models.TextField()
    image = models.ImageField(default='default.jpeg', upload_to='images/')
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'товары'

    def __str__(self):
        return self.title

