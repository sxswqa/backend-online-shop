from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from api_online_shop.api_shop.models import Category, SubCategory, Item
from api_online_shop.api_shop.serializers import CategorySerializer, SubCategorySerializer, ItemSerializer
from django.shortcuts import get_object_or_404


def get_category(pk):
    try:
        return Category.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise Http404


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = Category.objects.all()
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data)

    # permission_classes = [IsAuthenticated]
    @action(methods=['get'], detail=True)
    def get_list_of_subcategories(self, request, pk=None):
        category = get_category(pk)
        subcategories = SubCategory.objects.filter(category=category)
        serializer = SubCategorySerializer(subcategories, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def get_list_of_items_in_category(self, request, pk=None):
        category = get_category(pk)
        items = Item.objects.filter(subcategory__category=category)
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)


class SubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer

    def list(self, request, *args, **kwargs):
        subcategories = SubCategory.objects.all()
        serializer = SubCategorySerializer(subcategories, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def get_list_of_items_in_subcategory(self, request, pk=None):
        subcategory = SubCategory.objects.get(pk=pk)
        items = Item.objects.filter(subcategory=subcategory)
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def list(self, request, *args, **kwargs):
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)
